/****************************************************************************
 * gpsBin.js
 * January 2022
 *****************************************************************************/

const fs = require('fs');

const fftjs = require('./fft.js');
const precompCACodes = require('./caCodesPrecalculated.js');

const GPS_CODE_FREQUENCY = 1023000;
const INTERMEDIATE_FREQUENCY = 4092000;
const SAMPLE_RATE = 4092000;
const MS_IN_SEC = 1000;
const SAMPLE_LENGTH = (12 * SAMPLE_RATE / MS_IN_SEC);

const NAVSTAR_VALUES = [63, 56, 37, 35, 64, 36, 62, 44, 33, 38, 46, 59, 43, 49, 60, 51, 57, 50, 54, 47, 52, 53, 55, 23, 24, 26, 27, 48, 61, 39, 58, 22];

async function readFileC (filePath, callback) {

    fs.readFile(filePath, (err, data) => {

        if (err) {

            console.error(err);
            return;

        }

        const values = new Uint8Array(data.buffer);

        const rawSignal = new Array(values.length * 8);
        let index = 0;

        for (let i = 0; i < values.length; i++) {

            const value = values[i];

            for (let bit = 0; bit < 8; bit++) {

                rawSignal[index] = ((value >> bit) & 0x01) === 0x01 ? 1 : 0;

                index++;

            }

        }

        callback(rawSignal);

    });

}

function processSignal (rawSignal) {

    const FFT_LENGTH = Math.pow(2, Math.ceil(Math.log2(SAMPLE_LENGTH)));
    const f = new fftjs.FFT(FFT_LENGTH);

    const dopplerStart = Math.round(-5000 * FFT_LENGTH / SAMPLE_RATE);

    const data = f.createComplexArray();
    const prod = f.createComplexArray();

    console.log('SV    Nav    DOP     PHA     MaxSNR');

    for (let sv = 1; sv < 33; sv++) {

        const code = precompCACodes.caCodes[sv - 1];

        const caCode = f.createComplexArray();

        let caIndex = 0;
        let caPhase = 0;
        const caRate = GPS_CODE_FREQUENCY / SAMPLE_RATE;

        for (let i = 0; i < FFT_LENGTH; i++) {

            caCode[2 * i] = code[caIndex] ? -1 : 1;
            caCode[(2 * i) + 1] = 0;

            caPhase += caRate;

            if (caPhase >= 1) {

                caPhase -= 1;
                caIndex = (caIndex + 1) % code.length;

            }

        }

        // Run FFT on code

        const fftCode = f.createComplexArray();

        f.transform(fftCode, caCode);

        // Remove local oscillator from samples

        const loFreq = INTERMEDIATE_FREQUENCY;
        let loPhase = 0;
        const loRate = loFreq / SAMPLE_RATE * 4;

        const loSin = [1, 1, 0, 0];
        const loCos = [1, 0, 0, 1];

        for (let i = 0; i < FFT_LENGTH; i++) {

            if (i < SAMPLE_LENGTH) {

                data[2 * i] = (rawSignal[i] ^ loSin[loPhase]) ? -1 : 1;
                data[(2 * i) + 1] = (rawSignal[i] ^ loCos[loPhase]) ? -1 : 1;

                loPhase += loRate;
                if (loPhase >= 4) {

                    loPhase -= 4;

                }

            } else {

                data[2 * i] = 0;
                data[(2 * i) + 1] = 0;

            }

        }

        // Run FFT on data

        const fftData = f.createComplexArray();
        f.transform(fftData, data);

        // Test at different doppler shifts

        let maxSnr = 0;
        let maxSnrDop;
        let maxSnrIndex;

        for (let dop = dopplerStart; dop < 0; dop++) {

            let maxPower = 0;
            let totalPower = 0;

            let maxPowerIndex;

            // Complex multiply the C/A code spectrum with the spectrum that came from the data

            for (let i = 0; i < FFT_LENGTH; i++) {

                const j = (i - dop + FFT_LENGTH) % FFT_LENGTH;

                prod[2 * i] = fftData[2 * i] * fftCode[2 * j] + fftData[(2 * i) + 1] * fftCode[(2 * j) + 1];
                prod[(2 * i) + 1] = fftData[2 * i] * fftCode[(2 * j) + 1] - fftData[(2 * i) + 1] * fftCode[2 * j];

            }

            // Inverse FFT on prod

            const ifftProd = f.createComplexArray();
            f.inverseTransform(ifftProd, prod);

            for (let i = 0; i < 100; i++) {

                console.log('%i : %f %f', i, ifftProd[2 * i], ifftProd[(2 * i) + 1]);

            }

            return;

            // Look through result to find the point of max absolute power

            for (let i = 0; i < SAMPLE_RATE / MS_IN_SEC; i++) {

                const power = (ifftProd[2 * i] * ifftProd[2 * i]) + (ifftProd[(2 * i) + 1] * ifftProd[(2 * i) + 1]);

                if (power > maxPower) {

                    maxPower = power;
                    maxPowerIndex = i;

                }

                totalPower += power;

            }

            // Normalise units and find the maximum

            const avgPower = totalPower / SAMPLE_RATE * MS_IN_SEC;
            const snr = maxPower / avgPower;

            if (snr > maxSnr) {

                maxSnr = snr;
                maxSnrDop = dop;
                maxSnrIndex = maxPowerIndex;

            }

        }

        // Display results

        const doppler = -1 * maxSnrDop * SAMPLE_RATE / FFT_LENGTH;
        const phase = maxSnrIndex * GPS_CODE_FREQUENCY / SAMPLE_RATE;

        let stars = '';
        for (let i = Math.floor(maxSnr / 5.0 + 0.5); i > 0; i--) {

            stars += '*';

        }

        const navstar = NAVSTAR_VALUES[sv - 1];

        console.log(sv, ' ', navstar, ' ', doppler.toFixed(2), ' ', phase.toFixed(2), ' ', maxSnr.toFixed(1), ' ', stars);

    }

}

readFileC('./20211008_120340_000.bin', processSignal);
