/**********************************************************
* gps.c
*
* Modified from original by Andrew Holme
* http://www.holmea.demon.co.uk/GPS/Main.htm
*
* Requires Library FFTW - see http://www.fftw.org/
*
**********************************************************/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "fftw3.h"

#define GPS_CODE_FREQUENCY        1023000
#define INTERMEDIATE_FREQUENCY    4092000
#define SAMPLE_FREQUENCY          4092000
#define SAMPLE_LENGTH             (12 * SAMPLE_FREQUENCY / 1000)

/* Parameters for space vehicles */

const int SVs[] = {
     1,  63,  2,  6,
     2,  56,  3,  7,
     3,  37,  4,  8,
     4,  35,  5,  9,
     5,  64,  1,  9,
     6,  36,  2, 10,
     7,  62,  1,  8,
     8,  44,  2,  9,
     9,  33,  3, 10,
    10,  38,  2,  3,
    11,  46,  3,  4,
    12,  59,  5,  6,
    13,  43,  6,  7,
    14,  49,  7,  8,
    15,  60,  8,  9,
    16,  51,  9, 10,
    17,  57,  1,  4,
    18,  50,  2,  5,
    19,  54,  3,  6,
    20,  47,  4,  7,
    21,  52,  5,  8,
    22,  53,  6,  9,
    23,  55,  1,  3,
    24,  23,  4,  6,
    25,  24,  5,  7,
    26,  26,  6,  8,
    27,  27,  7,  9,
    28,  48,  8, 10,
    29,  61,  1,  6,
    30,  39,  2,  7,
    31,  58,  3,  8,
    32,  22,  4,  9,
};

/* Structure for C/A gold codes */

typedef struct {
    char g1[11], g2[11], *tap[2];
} CACODE;

void CACODE_init(CACODE *code, int t0, int t1) {
    code->tap[0] = code->g2 + t0;
    code->tap[1] = code->g2 + t1;
    memset(code->g1 + 1, 1, 10);
    memset(code->g2 + 1, 1, 10);
}

int CACODE_chip(CACODE *code) {
    return code->g1[10] ^ *code->tap[0] ^ *code->tap[1];
}

void CACODE_clock(CACODE *code) {
    code->g1[0] = code->g1[3] ^ code->g1[10];
    code->g2[0] = code->g2[2] ^ code->g2[3] ^ code->g2[6] ^ code->g2[8] ^ code->g2[9] ^ code->g2[10];
    memmove(code->g1 + 1, code->g1, 10);
    memmove(code->g2 + 1, code->g2, 10);
}

/* Code entry point */

int main(int argc, char *argv[]) {

    if (argc != 2) { puts("Use: ./gps FILENAME "); return 0; }

    /* Allocate space for FFT */

    fftw_complex *code = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * SAMPLE_LENGTH);
    fftw_complex *data = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * SAMPLE_LENGTH);
    fftw_complex *prod = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * SAMPLE_LENGTH);

    fftw_plan p;

    /* Read in the file file */

    FILE *fp = fopen(argv[1], "rb");

    if (!fp) { perror(argv[1]); return 0; }

    int byte;
    
    int index = 0;

    int *sample_data = (int*)malloc(SAMPLE_LENGTH * sizeof(int));

    for (int i = 0; i < SAMPLE_LENGTH / 8; i += 1) {

        int r = fread(&byte, 1, 1, fp);

        for (int j = 0; j < 8; j += 1) {

            sample_data[index] = byte & 0x01;

            index += 1;

            byte >>= 1;

        }

    }

    fclose(fp);

    /* Process all space vehicles in turn */

    printf("PRN Nav Doppler   Phase   MaxSNR\n");

    for (int sv=0; sv < sizeof(SVs) / sizeof(int); ) {

        int PRN     = SVs[sv++];
        int Navstar = SVs[sv++];
        int T1      = SVs[sv++];
        int T2      = SVs[sv++];

        if (!PRN) break;

        /* Generate the C/A code for the window of the data */

        CACODE ca;

        CACODE_init(&ca, T1, T2);

        double ca_freq = GPS_CODE_FREQUENCY, ca_phase = 0, ca_rate = ca_freq / SAMPLE_FREQUENCY;

        for (int i=0; i<SAMPLE_LENGTH; i++) {

            code[i][0] = CACODE_chip(&ca) ? -1 : 1;
            code[i][1] = 0;

            ca_phase += ca_rate;

            if (ca_phase >= 1) {
                ca_phase -= 1;
                CACODE_clock(&ca);
            }

        }
        
        /* Run the FFT on the C/A code stream  */

        p = fftw_plan_dft_1d(SAMPLE_LENGTH, code, code, FFTW_FORWARD, FFTW_ESTIMATE);

        fftw_execute(p);
        fftw_destroy_plan(p);

        /* Generate the same for the sample data, but removing the local oscillator from the samples */

        const int lo_sin[] = {1,1,0,0};
        const int lo_cos[] = {1,0,0,1};

        double lo_freq = INTERMEDIATE_FREQUENCY, lo_phase = 0, lo_rate = lo_freq / SAMPLE_FREQUENCY * 4;

        for (int i=0; i < SAMPLE_LENGTH; i++) {

            data[i][0] = (sample_data[i] ^ lo_sin[(int)lo_phase]) ? -1 : 1;
            data[i][1] = (sample_data[i] ^ lo_cos[(int)lo_phase]) ? -1 : 1;

            lo_phase += lo_rate;

            if (lo_phase >= 4) lo_phase -= 4;

        }

        p = fftw_plan_dft_1d(SAMPLE_LENGTH, data, data, FFTW_FORWARD, FFTW_ESTIMATE);

        fftw_execute(p);
        fftw_destroy_plan(p);

        /* Generate the execution plan for the inverse FFT (which will be reused multiple times) */

        p = fftw_plan_dft_1d(SAMPLE_LENGTH, prod, prod, FFTW_BACKWARD, FFTW_ESTIMATE);

        double max_snr = 0;

        int max_snr_dop, max_snr_i;

        /* Test at different doppler shifts (-5kHz to 0kHz) */

        for (int dop=-5000 * SAMPLE_LENGTH / SAMPLE_FREQUENCY; dop <= 0 * SAMPLE_LENGTH/SAMPLE_FREQUENCY; dop++) {

            double max_pwr = 0, tot_pwr = 0;
            
            int max_pwr_i;

            /* Complex multiply the C/A code spectrum with the spectrum that came from the data */

            for (int i=0; i<SAMPLE_LENGTH; i++) {

                int j = (i - dop + SAMPLE_LENGTH) % SAMPLE_LENGTH;
                
                prod[i][0] = data[i][0]*code[j][0] + data[i][1]*code[j][1];
                prod[i][1] = data[i][0]*code[j][1] - data[i][1]*code[j][0];
            
            }

            /* Run the inverse FFT */

            fftw_execute(p);

            /* Look through the result to find the point of max absolute power */

            for (int i = 0; i < SAMPLE_FREQUENCY / 1000; i++) {

                double pwr = prod[i][0]*prod[i][0] + prod[i][1]*prod[i][1];

                if (pwr>max_pwr) max_pwr=pwr, max_pwr_i = i;

                tot_pwr += pwr;

            }

            /* Normalise the units and find the maximum */

            double ave_pwr = tot_pwr / SAMPLE_FREQUENCY * 1000;
            
            double snr = max_pwr / ave_pwr;

            if (snr>max_snr) max_snr = snr, max_snr_dop = dop, max_snr_i = max_pwr_i;

        }

        fftw_destroy_plan(p);

        /* Display the result */

        printf("%-2d %4d %7.0f %8.2f %7.1f    ", PRN, Navstar, -max_snr_dop * (double)SAMPLE_FREQUENCY / SAMPLE_LENGTH, max_snr_i * (double)GPS_CODE_FREQUENCY / SAMPLE_FREQUENCY, max_snr);

        for (int i = (int)max_snr/10; i-- ;) putchar('*');

        putchar('\n');

    }

    fftw_free(code);
    fftw_free(data);
    fftw_free(prod);

    return 0;

}