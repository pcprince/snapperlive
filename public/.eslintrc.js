module.exports = {
    "extends": "standard",
    "rules": {
        "semi": [2, "always"],
        "indent": ["error", 4],
        "padded-blocks": ["error", "always"],
        "object-curly-spacing": ["error", "never"]
    }
};