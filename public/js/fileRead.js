/****************************************************************************
 * fileRead.js
 * January 2022
 *****************************************************************************/

/* global XMLHttpRequest */

/**
 * Read signal from file
 * @param {string} filePath Path to file
 * @param {function} callback Function to be called if file is successfully read
 */
async function readFile (filePath, callback) {

    let rawSignal;

    const req = new XMLHttpRequest();

    req.open('GET', filePath, true);
    req.responseType = 'arraybuffer';

    req.onload = () => {

        const values = new Uint8Array(req.response);

        rawSignal = new Array(values.length * 8);
        let index = 0;

        for (let i = 0; i < values.length; i++) {

            const value = values[i];

            for (let bit = 0; bit < 8; bit++) {

                rawSignal[index] = ((value >> bit) & 0x01) === 0x01 ? -1 : 1;

                index++;

            }

        }

        callback(rawSignal);

    };

    req.send(null);

}

async function readFileC (filePath, callback) {

    let rawSignal;

    const req = new XMLHttpRequest();

    req.open('GET', filePath, true);
    req.responseType = 'arraybuffer';

    req.onload = () => {

        const values = new Uint8Array(req.response);

        rawSignal = new Array(values.length * 8);
        let index = 0;

        for (let i = 0; i < values.length; i++) {

            const value = values[i];

            for (let bit = 0; bit < 8; bit++) {

                rawSignal[index] = ((value >> bit) & 0x01) === 0x01 ? 1 : 0;

                index++;

            }

        }

        callback(rawSignal);

    };

    req.send(null);

}
