/****************************************************************************
 * gpsAcquisition.js
 * January 2022
 *****************************************************************************/

/* global readFile, FFT, generateCACode, caCodes */

// UI elements

const testButton = document.getElementById('test-button');

// Constants

const MIN_DOPPLER = 0;
const MAX_DOPPLER = 7000;
const STEP_DOPPLER = 20;

const CODE_RATE = 1023000;
const CA_CODE_LENGTH = 1023;
const SAMPLE_RATE = 4092000;
const INTERMEDIATE_FREQUENCY = 4092000;

/**
 * Equally space num values between start and stop
 * @param {number} start Start value
 * @param {number} stop End value
 * @param {number} num Numbe rof values to be equally spaced between start and stop
 * @returns Array of linearly spaced values
 */
function linspace (start, stop, num) {

    const step = (stop - start) / (num - 1);
    return Array.from({length: num}, (_, i) => start + step * i);

}

/**
 * Concetenate an array with multiple copies of itself
 * @param {number[]} arr Array to be tiled
 * @param {number} repeats Number of times it should be tiles
 * @returns Array containing duplicated arrays
 */
function tile (arr, repeats) {

    return [].concat(...Array.from({length: repeats}, () => arr));

}

/**
 * Duplicate each value in an array (e.g. [1,2,3] -> [1,1,2,2,3,3])
 * @param {number[]} arr Array to be repeated
 * @param {number} repeats Number of times each value should be repeated
 * @returns Array containing duplicated values
 */
function repeat (arr, repeats) {

    const newArr = new Array(arr.length * repeats);

    for (let i = 0; i < arr.length; i++) {

        for (let j = 0; j < repeats; j++) {

            newArr[(i * repeats) + j] = arr[i];

        }

    }

    return newArr;

}

function describePower (sv, power) {

    // Loop through power and find x and y of peak

    let indexPower = 0; // x
    let indexCodePhase = 0; // y
    let peakPower = 0;

    let meanPower = 0;

    for (let i = 0; i < power.length; i++) {

        let totalPower = 0;

        for (let j = 0; j < power[0].length; j++) {

            if (power[i][j] > peakPower) {

                peakPower = power[i][j];
                indexPower = i;
                indexCodePhase = j;

            }

            totalPower += power[i][j];

        }

        meanPower += totalPower / power[0].length;

    }

    meanPower /= power.length;

    const codePhase = indexCodePhase / Math.floor(SAMPLE_RATE / CODE_RATE);

    const numberOfDopplerShifts = power[0].length;

    const dopplerShift = MIN_DOPPLER + indexPower * (MAX_DOPPLER - MIN_DOPPLER) / (numberOfDopplerShifts - 1);

    const peakSnr = peakPower / meanPower;

    let stars = '';
    for (let i = 0; i < Math.floor((peakSnr + 5) / 10); i++) {

        stars += '*';

    }

    console.log(sv, dopplerShift.toFixed(3), peakSnr.toFixed(3), stars);

}

/**
 * Perform GPS aquisition
 * @param {number[]} rawSignal Array of signal values
 */
function processSignal (rawSignal) {

    const timeStart = new Date();

    const fftLength = Math.pow(2, Math.ceil(Math.log2(rawSignal.length)));

    // Pad to fftLength

    const paddedRawSignal = Object.assign(new Array(fftLength).fill(0), rawSignal);

    // Apply FFT

    const f = new FFT(fftLength);

    const timeStartRawFFT = new Date();

    const fftRawSignal = f.createComplexArray();

    f.realTransform(fftRawSignal, paddedRawSignal);
    // f.completeSpectrum(fftRawSignal);

    const t = linspace(0, rawSignal.length / SAMPLE_RATE, rawSignal.length + 1);
    t.pop();

    const timeEndRawFFT = new Date();
    const diffRawFFT = timeEndRawFFT - timeStartRawFFT;
    console.log('Raw FFT time:', diffRawFFT);

    const timeStartCreate = new Date();

    const powerX = Math.floor(MAX_DOPPLER - MIN_DOPPLER) / STEP_DOPPLER + 1;
    const powerY = Math.floor(SAMPLE_RATE / CODE_RATE) * CA_CODE_LENGTH + 1;

    const power = new Array(powerX);

    for (let i = 0; i < powerX; i++) {

        power[i] = Array(powerY).fill(0);

    }

    const localSignal = new Array(fftLength * 2);
    localSignal.fill(0);

    const conj = new Array(fftRawSignal.length);

    const timeEndCreate = new Date();
    const diffCreate = timeEndCreate - timeStartCreate;
    console.log('Create array time:', diffCreate);

    let totalDoppler = 0;
    let totalCA = 0;

    let count = 0;
    let totalLocalSignal = 0;
    let totalLocalSignalFFT = 0;
    let totalConj = 0;
    let totalCorrelationFFT = 0;
    let totalCorrelationPower = 0;

    const timeStartUncoded = new Date();

    const uncodedLocalSignals = new Array(powerX);

    for (let i = 0; i < powerX; i++) {

        uncodedLocalSignals[i] = Array(fftLength * 2).fill(0);

    }

    for (let doppler = MIN_DOPPLER; doppler < MAX_DOPPLER + STEP_DOPPLER; doppler += STEP_DOPPLER) {

        const index = doppler / STEP_DOPPLER;

        const frequency = INTERMEDIATE_FREQUENCY + doppler;

        const angularStep = 2.0 * Math.PI * frequency / SAMPLE_RATE;

        let real = 1.0;
        let imaginary = 0.0;

        const cosAngularStep = Math.cos(angularStep);
        const sinAngularStep = Math.sin(angularStep);

        for (let i = 0; i < t.length; i++) {

            uncodedLocalSignals[index][2 * i] = real;
            uncodedLocalSignals[index][(2 * i) + 1] = imaginary;

            const newReal = (cosAngularStep * real) - (sinAngularStep * imaginary);
            const newImaginary = (sinAngularStep * real) + (cosAngularStep * imaginary);

            real = newReal;
            imaginary = newImaginary;

        }

    }

    const timeEndUncoded = new Date();
    const diffUncoded = timeEndUncoded - timeStartUncoded;

    console.log('SV', 'DOP', 'SNR');

    for (let sv = 1; sv < 33; sv++) {

        const timeStartCA = new Date();

        const code = caCodes[sv - 1];
        // const code = generateCACode(sv);

        const tiledCode = tile(code, Math.round(rawSignal.length / SAMPLE_RATE * 1000 + 0.5));

        const caCode = repeat(tiledCode, 4);
        caCode.length = rawSignal.length;

        const timeEndCA = new Date();
        const diffCA = timeEndCA - timeStartCA;
        totalCA += diffCA;

        const timeStartDoppler = new Date();

        for (let doppler = MIN_DOPPLER; doppler < MAX_DOPPLER + STEP_DOPPLER; doppler += STEP_DOPPLER) {

            count++;

            const index = doppler / STEP_DOPPLER;

            // Generate the FFT of the CA code for the window of the raw signal

            const timeStartLocalSignal = new Date();

            for (let i = 0; i < caCode.length; i++) {

                localSignal[2 * i] = caCode[i] * uncodedLocalSignals[index][2 * i];
                localSignal[(2 * i) + 1] = caCode[i] * uncodedLocalSignals[index][(2 * i) + 1];

            }

            const timeEndLocalSignal = new Date();
            const diffLocalSignal = timeEndLocalSignal - timeStartLocalSignal;
            totalLocalSignal += diffLocalSignal;

            const timeStartLocalSignalFFT = new Date();

            const fftLocalSignal = f.createComplexArray();

            f.transform(fftLocalSignal, localSignal);

            const timeEndLocalSignalFFT = new Date();
            const diffLocalSignalFFT = timeEndLocalSignalFFT - timeStartLocalSignalFFT;
            totalLocalSignalFFT += diffLocalSignalFFT;

            // Calculate conjugate and multiply it by fftLocalSignal

            const timeStartConj = new Date();

            const fftRawSignalLen = fftLength * 2;

            for (let i = 0; i < fftRawSignalLen; i++) {

                if (i % 2) {

                    const a = fftLocalSignal[i - 1];
                    const b = fftLocalSignal[i];
                    const c = fftRawSignal[i - 1];
                    const d = -1.0 * fftRawSignal[i]; // Use the conjugate of fftRawSignal

                    // Perform complex multiplication

                    conj[i - 1] = (a * c) - (b * d);
                    conj[i] = (a * d) + (b * c);

                }

            }

            const timeEndConj = new Date();
            const diffConj = timeEndConj - timeStartConj;
            totalConj += diffConj;

            // Perform inverse FFT on result

            const timeStartCorrelationFFT = new Date();

            const correlation = f.createComplexArray();

            f.inverseTransform(correlation, conj);

            correlation.length = 2 * powerY;

            const timeEndCorrelationFFT = new Date();
            const diffCorrelationFFT = timeEndCorrelationFFT - timeStartCorrelationFFT;
            totalCorrelationFFT += diffCorrelationFFT;

            const timeStartCorrelationPower = new Date();

            for (let i = 0; i < powerY; i++) {

                power[index][i] = (correlation[2 * i] * correlation[2 * i]) + (correlation[(2 * i) + 1] * correlation[(2 * i) + 1]);

            }

            const timeEndCorrelationPower = new Date();
            const diffCorrelationPower = timeEndCorrelationPower - timeStartCorrelationPower;
            totalCorrelationPower += diffCorrelationPower;

        }

        const timeEndDoppler = new Date();
        const diffDoppler = timeEndDoppler - timeStartDoppler;
        totalDoppler += diffDoppler;

        describePower(sv, power);

    }

    const timeEnd = new Date();
    const diff = timeEnd - timeStart;

    const avgDoppler = totalDoppler / 32.0;
    const avgCA = totalCA / 32.0;
    const avgLocalSignal = totalLocalSignal / count;
    const avgLocalSignalFFT = totalLocalSignalFFT / count;
    const avgConj = totalConj / count;
    const avgCorrelationFFT = totalCorrelationFFT / count;
    const avgCorrelationPower = totalCorrelationPower / count;

    console.log('Average CA time:', avgCA);
    console.log('Uncoded prep time:', diffUncoded);
    console.log('--');
    console.log('Average doppler time:', avgDoppler, ',', 'Doppler loops:', count);
    console.log('-');
    console.log('Average local signal time:', avgLocalSignal);
    console.log('Average local signal FFT time:', avgLocalSignalFFT);
    console.log('Average conj:', avgConj);
    console.log('Average correlation FFT:', avgCorrelationFFT);
    console.log('Average correlation power:', avgCorrelationPower);
    console.log('-');
    console.log('Total time:', diff, 'ms,', diff / 1000 / 60, 'minutes');

}

testButton.addEventListener('click', () => {

    readFile('./testFiles/20211003_205220_000.bin', processSignal);

});

// TODO: Run in background with JS worker thread
