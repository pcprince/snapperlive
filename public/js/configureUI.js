/****************************************************************************
 * configureUI.js
 * April 2021
 *****************************************************************************/

/* global  requestDevice, isDeviceAvailable, resetDeviceInfo, setDisconnectFunction, connectToDevice, updateCache */

const pairButton = document.getElementById('pair-button');

const errorDisplay = document.getElementById('error-display');
const errorText = document.getElementById('error-text');

/**
 * Looping function which checks for presence of WebUSB device
 */
function checkForDevice (repeat = true) {

    if (isDeviceAvailable()) {

        pairButton.disabled = true;

    } else {

        resetDeviceInfo();

    }

    if (repeat) {

        setTimeout(checkForDevice, 500);

    }

}

/**
 * Report an error to the user
 * @param {string} err Error text to be shown to the user
 */
function displayError (err) {

    console.error(err);

    errorDisplay.style.display = '';
    errorText.innerHTML = err;

    window.scrollTo(0, 0);

}

// Connect to a WebUSB device

pairButton.addEventListener('click', () => {

    requestDevice((err) => {

        if (err) {

            displayError(err);

        } else {

            errorDisplay.style.display = 'none';

        }

    });

});

// Set the function which will be called when a WebUSB device is disconnected

setDisconnectFunction(() => {

    // Wipe the device information panel

    resetDeviceInfo();

});

// Let start date/time default to next hour
const dateTime = new Date();
dateTime.setHours(dateTime.getHours() + Math.ceil(dateTime.getMinutes() / 60));

if (!navigator.usb) {

    pairButton.disabled = true;

} else {

    // Check to see if a device is already connected

    checkForDevice(true);

    connectToDevice(true);

}

updateCache();
