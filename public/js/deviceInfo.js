/****************************************************************************
 * deviceInfo.js
 * April 2021
 *****************************************************************************/

/* global strftime, device, AM_USB_MSG_TYPE_GET_INFO */

const strftimeUTC = strftime.utc();

// Information which can be pulled from the device

var time = null;
var milliseconds = null;
var batteryVoltage = null;
var firmwareDescription = null;
var firmwareVersion = [null, null, null];
var statusString = null;
var snapshotCount = null;
var deviceID = null;

// UI elements

const deviceIDSpan = document.getElementById('device-id-span');
const timeSpan = document.getElementById('time-span');
const batteryVoltageSpan = document.getElementById('battery-voltage-span');
const firmwareDescriptionSpan = document.getElementById('firmware-description-span');
const firmwareVersionSpan = document.getElementById('firmware-version-span');
const snapshotCountSpan = document.getElementById('snapshot-count-span');
const statusSpan = document.getElementById('status-span');

/**
 * Reset UI elements to placeholder values
 */
function resetDeviceInfo () {

    timeSpan.innerHTML = '-';
    batteryVoltageSpan.innerHTML = '-';
    firmwareDescriptionSpan.innerHTML = '-';
    firmwareVersionSpan.innerHTML = '-';
    snapshotCountSpan.innerHTML = '-';
    statusSpan.innerHTML = '-';
    deviceIDSpan.innerHTML = '-';

    time = null;
    milliseconds = null;
    batteryVoltage = null;
    firmwareDescription = null;
    // temp = null;
    firmwareVersion = [null, null, null];
    statusString = null;
    snapshotCount = null;
    deviceID = null;

}

/**
 * Use collected device information to fill in UI elements
 */
function updateDeviceInfo () {

    if (deviceID !== null) {

        deviceIDSpan.innerHTML = deviceID.toString(16).toUpperCase();

    } else {

        deviceIDSpan.innerHTML = '-';

    }

    if (time !== null) {

        timeSpan.innerHTML = strftimeUTC('%Y-%m-%d %H:%M:%S', new Date(time * 1000)) +
                             '.' + ('0000' + milliseconds).slice(-3) + ' (UTC)';

    } else {

        timeSpan.innerHTML = '-';

    }

    if (batteryVoltage !== null) {

        batteryVoltageSpan.innerHTML = (batteryVoltage).toFixed(2) + ' V';

    } else {

        batteryVoltageSpan.innerHTML = '-';

    }

    if (firmwareDescription !== null) {

        firmwareDescriptionSpan.innerHTML = firmwareDescription;

    } else {

        firmwareDescriptionSpan.innerHTML = '-';

    }

    if (firmwareVersion[0] !== null) {

        firmwareVersionSpan.innerHTML = firmwareVersion[0] + '.' + firmwareVersion[1] + '.' + firmwareVersion[2];

    } else {

        firmwareVersionSpan.innerHTML = '-';

    }

    if (snapshotCount !== null) {

        snapshotCountSpan.innerHTML = snapshotCount.toString();

    } else {

        snapshotCountSpan.innerHTML = '-';

    }

    if (statusString !== null) {

        statusSpan.innerHTML = statusString;

    } else {

        statusSpan.innerHTML = '-';

    }

}

/**
 * Request information from a connected Snapper device
 */
async function getDeviceInformation () {

    if (device) {

        const data = new Uint8Array([AM_USB_MSG_TYPE_GET_INFO]);

        try {

            // Send request packet and wait for response

            let result = await device.transferOut(0x01, data);

            result = await device.transferIn(0x01, 64);

            // Read device time
            time = result.data.getUint8(1) + 256 * (result.data.getUint8(2) + 256 * (result.data.getUint8(3) + 256 * result.data.getUint8(4)));
            milliseconds = Math.round((result.data.getUint8(5) + 256 * result.data.getUint8(6)) / 1024 * 1000);

            // Temperature in tenths of degrees kelvin
            // Convert to degrees celsius
            // const tempK = result.data.getUint8(5) + 256 * (result.data.getUint8(6) + 256 * (result.data.getUint8(7) + 256 * result.data.getUint8(8)));
            // temp = ((tempK - 2731.5) / 10);

            // Read device state
            switch (result.data.getUint8(9)) { // TODO: -> 56

            case 0:
                statusString = 'Will shutdown';
                break;
            case 1:
                statusString = 'Will record';
                break;
            case 2:
                statusString = 'Erasing';
                break;
            default:
                statusString = 'Undefined';

            }

            // Read battery voltage
            batteryVoltage = (result.data.getUint8(10) + 256 * (result.data.getUint8(11) + 256 * (result.data.getUint8(12) + 256 * result.data.getUint8(13)))) / 100;
            // TODO: -> 9, 10, 11, 12

            // Read device ID
            deviceID = BigInt(0);
            for (let i = 21; i >= 14; --i) { // TODO: -> 20..13

                deviceID *= BigInt(256);
                deviceID += BigInt(result.data.getUint8(i));

            }

            // Read firmware
            firmwareDescription = '';
            for (let i = 22; i < 54; ++i) { // TODO: -> 21..53

                firmwareDescription += String.fromCharCode(result.data.getUint8(i));

            }

            firmwareVersion = [];
            for (let i = 54; i < 57; ++i) { // TODO: -> 53..56

                firmwareVersion.push(result.data.getUint8(i));

            }

            // Read number of snapshots on device
            snapshotCount = result.data.getUint8(57) + 256 * (result.data.getUint8(58));

            updateDeviceInfo();

        } catch (err) {

            console.error(err);

            resetDeviceInfo();

        }

    }

}
