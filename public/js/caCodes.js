/****************************************************************************
 * caCodes.js
 * January 2022
 *****************************************************************************/

const SELECTION = [
    [2, 6],
    [3, 7],
    [4, 8],
    [5, 9],
    [1, 9],
    [2, 10],
    [1, 8],
    [2, 9],
    [3, 10],
    [2, 3],
    [3, 4],
    [5, 6],
    [6, 7],
    [7, 8],
    [8, 9],
    [9, 10],
    [1, 4],
    [2, 5],
    [3, 6],
    [4, 7],
    [5, 8],
    [6, 9],
    [1, 3],
    [4, 6],
    [5, 7],
    [6, 8],
    [7, 9],
    [8, 10],
    [1, 6],
    [2, 7],
    [3, 8],
    [4, 9]
];

/**
 * A simple shift register
 * @param {number[]} register register
 * @param {number[]} feedbackPosition feedbackPosition
 * @param {number[]} outputPositions outputPositions
 * @returns Output
 */
function shift (register, feedbackPosition, outputPositions) {

    let out = new Array(outputPositions.length);

    let outSum = 0;

    for (let i = 0; i < outputPositions.length; i++) {

        out[i] = register[outputPositions[i] - 1];
        outSum += out[i];

    }

    if (out.length > 1) {

        out = outSum % 2;

    } else {

        out = out[0];

    }

    let fb = 0;

    for (let i = 0; i < feedbackPosition.length; i++) {

        fb += register[feedbackPosition[i] - 1];

    }

    fb = fb % 2;

    for (let i = register.length - 2; i >= 0; i--) {

        register[i + 1] = register[i];

    }

    register[0] = fb;

    return out;

}

/**
 * Calculates the C/A code of a gps satellite
 * @param {number} satelliteNumber GPS satellite number between and including 1 and 32
 * @returns The C/A code of the specified satellite as a list of -1s and +1s, e.g. [+1, -1, +1, ... , +1]
 */
function generateCACode (satelliteNumber) {

    // Satellite number comes in as 1 - 32, reduce it to range of array

    satelliteNumber--;

    const G1 = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
    const G2 = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1];

    const ca = [];

    for (let i = 0; i < 1023; i++) {

        const g1 = shift(G1, [3, 10], [10]);
        const g2 = shift(G2, [2, 3, 6, 8, 9, 10], SELECTION[satelliteNumber]);

        ca[i] = (g1 + g2) % 2;

    }

    // Turn (0, 1) to (-1, +1)

    for (let i = 0; i < ca.length; i++) {

        ca[i] = ca[i] === 0 ? -1 : ca[i];

    }

    return ca;

}
