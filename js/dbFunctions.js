/****************************************************************************
 * dbFunctions.js
 * March 2021
 *****************************************************************************/

const crypto = require('crypto');

// Reference location
// Roof of RHB
const latRef = 51.75900280;
const lngRef = -1.25653900;
// RHB antennas
// const latRef = 51.75897846675064;
// const lngRef = -1.2564937431493317;
// 7 Kirby Place driveway
// const latRef = 51.735420;
// const lngRef = -1.210994;
// 7 Kirby Place window
// const latRef = 51.735449;
// const lngRef = -1.210985;
// HBAC
// const latRef = 51.760715;
// const lngRef = -1.261047;

// Number of snapshots per static test set
const snapshotsPerSet = 10;

/**
 * Generate a random, alphanumeric ID of length 10
 */
function generateID () {

    return crypto.randomBytes(5).toString('hex');

}

/**
 * Add a row to the Uploads table
 * @param {object} dbClient Postgres client
 * @param {string} deviceID Unique, 16 character internal EFM32 serial number
 * @param {string} email User email address
 * @param {string} subscription User push notification subscription
 * @param {float} maxVelocity Maximum receiver velocity
 * @param {function} callback Function called on completion
 */
exports.addUpload = (dbClient, deviceID, email, subscription, maxVelocity, callback) => {

    const now = new Date();
    const year = now.getFullYear();
    const month = now.getMonth() + 1;
    const day = now.getDate();
    const hours = now.getHours();
    const minutes = now.getMinutes();
    const seconds = now.getSeconds();
    const msString = now.getMilliseconds().toString().padStart(3, '0');

    let dtString = year.toString();
    dtString += '-';
    dtString += month.toString();
    dtString += '-';
    dtString += day.toString();
    dtString += ' ';
    dtString += hours.toString();
    dtString += ':';
    dtString += minutes.toString();
    dtString += ':';
    dtString += seconds.toString();
    dtString += '.';
    dtString += msString;

    maxVelocity = (!isNaN(parseFloat(maxVelocity)) && isFinite(maxVelocity)) ? maxVelocity : null;

    // Earliest processing date defaults to the UNIX epoch, then gets updated once all the snapshots have been looked at and the earliest date calculated

    dbClient.query('INSERT INTO uploads(upload_id, device_id, status, earliest_processing_date, datetime, email, subscription, max_velocity) VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING upload_id', [generateID(), deviceID, 'uploading', '1970-01-01 00:00:00', dtString, email, subscription, maxVelocity], callback);

};

/**
 * Create a new reference point associated with a given upload ID
 * @param {object} dbClient Postgres client
 * @param {string} uploadID Unique upload ID
 * @param {float} lat Latitiude
 * @param {float} lng Longitude
 * @param {string} timeString UTC string of the date/time when the reference point was collected
 * @param {function} callback Function called on completion
 */
exports.addReferencePoint = (dbClient, uploadID, lat, lng, timeString, callback) => {

    const values = [lat, lng, timeString, uploadID];

    dbClient.query('INSERT INTO reference_points(reference_id, lat, lng, datetime, upload_id) VALUES (DEFAULT, $1, $2, $3, $4)', values, callback);

};

/**
 * Get all reference points associated with an upload
 * @param {object} dbClient Postgres client
 * @param {string} uploadID Unique upload ID
 * @param {function} callback Function called on completion
 */
exports.getReferencePoints = (dbClient, uploadID, callback) => {

    dbClient.query('SELECT lat, lng, datetime FROM reference_points WHERE upload_id = $1', [uploadID], callback);

};

/**
 * Get the contents of the uploads table
 * @param {object} dbClient Postgres client
 * @param {function} callback Function called on completion
 */
exports.listUploads = (dbClient, callback) => {

    const selectQuery = 'SELECT * FROM uploads';

    dbClient.query(selectQuery, callback);

};

/**
 * Add a row to the Snapshots table
 * @param {object} dbClient Postgres client
 * @param {string} uploadID Unique upload ID
 * @param {string} deviceID Unique, 16 character internal EFM32 serial number
 * @param {object} datetime Datetime object representing estimated time snapshot was taken
 * @param {number} battery Floating point number representing battery level at time of snapshot
 * @param {number} hxfoCount HXFO count
 * @param {number} lxfoCount LXFO count
 * @param {number} temperature Floating point number representing temperature at time of snapshot
 * @param {object} buff Buffer containing snapshot data
 * @param {function} callback Function called on completion
 */
exports.addSnapshot = (dbClient, uploadID, datetime, battery, hxfoCount, lxfoCount, temperature, buff, callback) => {

    const year = datetime.getFullYear();
    const month = datetime.getMonth() + 1;
    const day = datetime.getDate();
    const hours = datetime.getHours();
    const minutes = datetime.getMinutes();
    const seconds = datetime.getSeconds();
    const msString = datetime.getMilliseconds().toString().padStart(3, '0');

    let dtString = year.toString();
    dtString += '-';
    dtString += month.toString();
    dtString += '-';
    dtString += day.toString();
    dtString += ' ';
    dtString += hours.toString();
    dtString += ':';
    dtString += minutes.toString();
    dtString += ':';
    dtString += seconds.toString();
    dtString += '.';
    dtString += msString;

    /* Append a double-escaped x to announce to postgres that bytea data is in the hex format, rather than escaped */
    const header = Buffer.from('\\x');
    const hexBuffer = Buffer.concat([header, buff]);

    const values = [uploadID, dtString, battery, hxfoCount, lxfoCount, temperature, hexBuffer];

    dbClient.query('INSERT INTO snapshots(snapshot_id, upload_id, datetime, battery, hxfo_count, lxfo_count, temperature, data) VALUES (DEFAULT, $1, $2, $3, $4, $5, $6, $7)', values, callback);

};

/**
 * For an existing upload, change the status to one of the predefined enum values
 * @param {object} dbClient Postgres client
 * @param {string} uploadID Unique upload ID
 * @param {string} status New status using the following enums: 'uploading', 'waiting', 'processing', 'complete'
 * @param {function} callback Function called on completion
 */
exports.updateUploadStatus = (dbClient, uploadID, status, callback) => {

    dbClient.query('UPDATE uploads SET status = $1 WHERE upload_id = $2 RETURNING *', [status, uploadID], callback);

};

/**
 * Update the date which an upload should be processed
 * @param {object} dbClient Postgres client
 * @param {string} uploadID Unique upload ID
 * @param {string} earliestProcessingDateString UTC string of the date/time of the earliest point an upload can be processed
 * @param {function} callback Function called upon completion
 */
exports.updateUploadProcessingDate = (dbClient, uploadID, earliestProcessingDateString, callback) => {

    dbClient.query('UPDATE uploads SET earliest_processing_date = $1 WHERE upload_id = $2 RETURNING *', [earliestProcessingDateString, uploadID], callback);

};

/**
 * Delete an existing upload and all associated snapshots (table is set to CASCADE DELETE)
 * @param {object} dbClient Postgres client
 * @param {string} uploadID Unique upload ID
 * @param {function} callback Fucntion called on completion
 */
exports.deleteUpload = (dbClient, uploadID, callback) => {

    dbClient.query('DELETE FROM uploads WHERE upload_id = $1', [uploadID], callback);

};

/**
 * Request the information about an upload needed to process a snapshot
 * @param {object} dbClient Postgres client
 * @param {string} uploadID Unique upload ID
 * @param {function} callback Function called on completion
 */
exports.getUploadInformation = (dbClient, uploadID, callback) => {

    dbClient.query('SELECT datetime, device_id FROM uploads WHERE upload_id = $1', [uploadID], callback);

};

/**
 * Get the snapshot_id of the snapshots associated with a given upload
 * @param {object} dbClient Postgres client
 * @param {string} uploadID Unique upload ID
 * @param {function} callback Function called on completion
 */
exports.getSnapshotInformation = (dbClient, uploadID, callback) => {

    dbClient.query('SELECT snapshot_id FROM snapshots WHERE upload_id = $1', [uploadID], callback);

};

/**
 * Given an upload ID, return an array of position objects including the GPS position and a corrected timestamp
 * @param {object} dbClient Postgres client
 * @param {string} uploadID Unique upload ID
 * @param {function} callback Function called on completion
 */
exports.getPositions = async (dbClient, uploadID, callback) => {

    const positions = [];

    // Get all snapshots associated with the given upload ID

    const response = await dbClient.query('SELECT * FROM (SELECT DISTINCT ON (temp.snapshot_id) datetime, temperature, battery, estimated_lat, estimated_lng, estimated_time_correction, estimated_horizontal_error FROM (SELECT snapshot_id, datetime, temperature, battery FROM snapshots WHERE upload_id = $1) AS temp INNER JOIN positions ON temp.snapshot_id=positions.snapshot_id ORDER BY temp.snapshot_id, position_id DESC) as temp2 ORDER BY datetime ASC', [uploadID]);

    for (let i = 0; i < response.rows.length; i++) {

        positions.push({
            estimated_lat: response.rows[i].estimated_lat,
            estimated_lng: response.rows[i].estimated_lng,
            timestamp: (response.rows[i].datetime.getTime() / 1000) + response.rows[i].estimated_time_correction,
            temperature: response.rows[i].temperature,
            battery: response.rows[i].battery,
            estimated_horizontal_error: response.rows[i].estimated_horizontal_error
        });

    }

    callback(false, positions);

};

/**
 * Get summary of accuracy of receiver connected to Raspberry Pi in RHB.
 * @param {object} dbClient Postgres client
 * @param {function} callback Function called on completion
 */
exports.getRaspberryPiAccuracy = async (dbClient, yearMonth, callback) => {

    try {

        // Get all uploads form the Pi that are completely processed
        const response = await dbClient.query('SELECT datetime, upload_id FROM uploads WHERE device_id = \'Raspberry_Pi_RHB\' AND status = \'complete\' AND EXTRACT(YEAR FROM datetime) = $1 AND EXTRACT(MONTH FROM datetime) = $2 ORDER BY datetime DESC', [parseInt(yearMonth.slice(0, 4)), parseInt(yearMonth.slice(-2))]);

        const goodRatioArray = [[], []];

        const medianArray = [[], []];

        const datetimeArray = [];

        // Loop over all uploads
        for (let i = 0; i < response.rows.length; ++i) {

            const uploadID = response.rows[i].upload_id;

            datetimeArray.push(response.rows[i].datetime);

            // Get all snapshots that belong to this upload
            const responseSnapshot = await dbClient.query('SELECT estimated_lat, estimated_lng, estimated_horizontal_error FROM positions WHERE snapshot_id IN (SELECT snapshot_id FROM snapshots WHERE upload_id = $1) ORDER BY position_id ASC', [uploadID]);

            // Iterate over real-time data and post-processed data
            for (let k = 0; k < 2; ++k) {

                const errors = [];

                let goodFixes = 0;
                let nFixes = 0;

                // Loop over all snapshots that belong to this upload
                for (let j = k * snapshotsPerSet;
                    j < responseSnapshot.rows.length &&
                    j < (k + 1) * snapshotsPerSet;
                    ++j) {

                    const lat = responseSnapshot.rows[j].estimated_lat;
                    const lng = responseSnapshot.rows[j].estimated_lng;

                    ++nFixes;

                    // Check if geodetic coordinates are valid
                    if (!isNaN(lat) && !isNaN(lng) &&
                        !(lat === null) && !(lng === null) &&
                        !(lat === undefined) && !(lng === undefined) &&
                        lat > -90.0 && lat < 90.0 &&
                        lng > -180.0 && lng < 180.0) {

                        // Geodetic coordinates to ENU coordinates (works only for Oxford)
                        const east = (lng - lngRef) / 0.001 * 69.04731745495504;
                        const north = (lat - latRef) / 0.001 * 111.26452406761157;

                        // Horizontal error
                        errors.push(Math.sqrt(Math.pow(east, 2) + Math.pow(north, 2)));

                        const confidence = responseSnapshot.rows[j].estimated_horizontal_error;

                        // Check if confidence is small
                        if (!isNaN(confidence) &&
                            !(confidence === null) &&
                            !(confidence === undefined) &&
                            confidence < 200) ++goodFixes;

                    } else {

                        errors.push(Infinity);

                    }

                }

                let median = null;

                if (errors.length > 0) {

                    errors.sort(function (a, b) {

                        return a - b;

                    });

                    var half = Math.floor(errors.length / 2);

                    if (errors.length % 2) {

                        median = errors[half];

                    } else {

                        median = (errors[half - 1] + errors[half]) / 2.0;

                    }

                }

                goodRatioArray[k].push(goodFixes / nFixes);
                medianArray[k].push(median);

            }

        }

        callback(false, {

            datetimeArray: datetimeArray,
            goodRatioArray: goodRatioArray[0], // results from real-time processing
            medianArray: medianArray[0], // results from real-time processing
            goodRatioArrayPost: goodRatioArray[1], // results from post-processing
            medianArrayPost: medianArray[1] // results from post-processing

        });

    } catch (err) {

        callback(err);

    }

};
