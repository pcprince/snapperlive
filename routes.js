/****************************************************************************
 * routes.js
 * March 2021
 *****************************************************************************/

module.exports = function (app, dbClient) {

    /**
     * Render the homepage
     */
    app.get('/', (req, res) => {

        res.render('index');

    });

};
